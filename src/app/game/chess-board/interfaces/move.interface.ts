export interface Move {
  color: 'white' | 'black';
  timestamp: number;
}
