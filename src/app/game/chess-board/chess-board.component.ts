import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Chessman } from '../chessman/interfaces/chessman.interface';
import { Position } from './interfaces/position.interface';
import { Move } from './interfaces/move.interface';
import { range } from 'lodash';

const DEFAULT_BOARD_SIZE = 8;

@Component({
  selector: 'app-chess-board',
  templateUrl: './chess-board.component.html',
  styleUrls: ['./chess-board.component.less'],
})
export class ChessBoardComponent implements OnInit {
  @Input()
  boardSize: number = DEFAULT_BOARD_SIZE;
  @Input()
  playerColor: 'white' | 'black' = 'white';

  boardState: (Chessman | null)[][] = [];
  targetedChessman: Position | null = null;
  turnOrder: 'white' | 'black' = 'white';

  private moves: Move[] = [];

  get isEnemyTurn(): boolean {
    return this.turnOrder !== this.playerColor;
  }

  ngOnInit(): void {
    this.initBoard();
  }

  showCanMoveHint(position: Position): boolean {
    return !!this.targetedChessman && this.canMakeMove(position);
  }

  // Return true if there's a chessman in the cell or some chessman targeted and we're hovering on black cell
  isClickableCell(chessman: Chessman | null, position: Position): boolean {
    return (
      !!chessman ||
      (!!this.targetedChessman &&
        (position.columnIndex + position.rowIndex) % 2 !== 0)
    );
  }

  getCellColor(row: number, column: number): 'white' | 'black' {
    return (row + column) % 2 ? 'black' : 'white';
  }

  onCellClick(chessmanInCell: Chessman | null, position: Position): void {
    if (!chessmanInCell) {
      if (this.targetedChessman) {
        const canMakeFatalMove = this.boardState.some((row, rowInd) =>
          row.some(
            (column, columnInd) =>
              column &&
              column.color === this.turnOrder &&
              this.checkCanMakeFatalMove({
                rowIndex: rowInd,
                columnIndex: columnInd,
              })
          )
        );
        const isMakingFatalMove = this.removeEnemies(
          this.targetedChessman!,
          position,
          false
        );

        if (this.canMakeMove(position)) {
          if ((canMakeFatalMove && isMakingFatalMove) || !canMakeFatalMove) {
            this.makeMove(position);
          }
        }
      }
    } else {
      this.targetChessman(chessmanInCell, position);
    }
  }

  canMakeMove(
    { rowIndex: moveRowInd, columnIndex: moveColumnInd }: Position,
    from?: Position
  ): boolean {
    if (
      moveRowInd < 0 ||
      moveColumnInd < 0 ||
      moveRowInd + 1 > this.boardSize ||
      moveColumnInd + 1 > this.boardSize
    )
      return false;

    if (!this.targetedChessman || this.boardState[moveRowInd][moveColumnInd])
      return false;

    const { rowIndex: curRowInd, columnIndex: curColumnInd } =
      from ?? this.targetedChessman;
    const curColor = this.boardState[curRowInd][curColumnInd]?.color!;
    const curKingState = this.boardState[curRowInd][curColumnInd]?.isKing!;

    if (!curKingState) {
      if (curColor === 'black') {
        // Can go only with higher rowIndex (except attack case)
        if (moveRowInd - curRowInd === 1) {
          // Normal case
          return Math.abs(moveColumnInd - curColumnInd) === 1;
        } else if (moveRowInd - curRowInd === 2) {
          // Attack case
          return (
            (moveColumnInd > curColumnInd &&
              this.boardState[moveRowInd - 1][moveColumnInd - 1]?.color ==
                'white') ||
            (moveColumnInd < curColumnInd &&
              this.boardState[moveRowInd - 1][moveColumnInd + 1]?.color ==
                'white')
          );
        } else if (curRowInd - moveRowInd === 2) {
          // Attack case
          return (
            (moveColumnInd > curColumnInd &&
              this.boardState[moveRowInd + 1][moveColumnInd - 1]?.color ==
                'white') ||
            (moveColumnInd < curColumnInd &&
              this.boardState[moveRowInd + 1][moveColumnInd + 1]?.color ==
                'white')
          );
        }
      } else {
        // Can go only with lower rowIndex (except attack case)
        if (curRowInd - moveRowInd === 1) {
          // Normal case
          return Math.abs(moveColumnInd - curColumnInd) === 1;
        } else if (moveRowInd - curRowInd === 2) {
          // Attack case
          return (
            (moveColumnInd > curColumnInd &&
              this.boardState[moveRowInd - 1][moveColumnInd - 1]?.color ==
                'black') ||
            (moveColumnInd < curColumnInd &&
              this.boardState[moveRowInd - 1][moveColumnInd + 1]?.color ==
                'black')
          );
        } else if (curRowInd - moveRowInd === 2) {
          // Attack case
          return (
            (moveColumnInd > curColumnInd &&
              this.boardState[moveRowInd + 1][moveColumnInd - 1]?.color ==
                'black') ||
            (moveColumnInd < curColumnInd &&
              this.boardState[moveRowInd + 1][moveColumnInd + 1]?.color ==
                'black')
          );
        }
      }
    } else {
      let twoEnemyChessmanStayTogether = false;
      let sameColorOnWay = false;

      const diagonal = this.getDiagonalPositions(
        {
          rowIndex: curRowInd,
          columnIndex: curColumnInd,
        },
        { rowIndex: moveRowInd, columnIndex: moveColumnInd }
      );

      for (let i = 1; i < diagonal.length - 1; i++) {
        const currentCell =
          this.boardState[diagonal[i].rowIndex][diagonal[i].columnIndex];
        const prevCell =
          this.boardState[diagonal[i - 1].rowIndex][
            diagonal[i - 1].columnIndex
          ];

        if (currentCell) {
          if (currentCell.color === this.turnOrder) {
            sameColorOnWay = true;
          } else if (prevCell && prevCell.color !== this.turnOrder) {
            twoEnemyChessmanStayTogether = true;
          }
        }
      }

      // Diagonal move of king
      return (
        Math.abs(moveColumnInd - curColumnInd) ===
          Math.abs(moveRowInd - curRowInd) &&
        !twoEnemyChessmanStayTogether &&
        !sameColorOnWay
      );
    }

    return false;
  }

  isTargetedPosition({ rowIndex, columnIndex }: Position): boolean {
    return (
      !!this.targetedChessman &&
      this.targetedChessman.rowIndex === rowIndex &&
      this.targetedChessman.columnIndex === columnIndex
    );
  }

  private makeMove(position: Position): void {
    let wasDestroyed = false;
    const { rowIndex: curRowInd, columnIndex: curColumnInd } =
      this.targetedChessman!;
    const currentChessman = this.boardState[curRowInd][curColumnInd];

    this.boardState[position.rowIndex][position.columnIndex] = currentChessman;
    this.boardState[curRowInd][curColumnInd] = null;
    this.targetedChessman = position;
    this.moves.push({ color: this.turnOrder, timestamp: Date.now() });

    if (
      this.removeEnemies(
        { rowIndex: curRowInd, columnIndex: curColumnInd },
        position
      )
    ) {
      wasDestroyed = true;
    }

    // Mark as king
    if (
      currentChessman &&
      ((currentChessman.color === 'white' && position.rowIndex === 0) ||
        (currentChessman.color === 'black' &&
          position.rowIndex === this.boardSize - 1))
    ) {
      this.boardState[position.rowIndex][position.columnIndex] = {
        ...currentChessman,
        isKing: true,
      };
    }

    if (!wasDestroyed || !this.checkCanMakeFatalMove()) {
      // In case we can't make any additional move
      this.swapTurn();
    }
  }

  private getDiagonalPositions(from: Position, to: Position): Position[] {
    const rowCoord = [...range(from.rowIndex, to.rowIndex), to.rowIndex];
    const colCoord = [
      ...range(from.columnIndex, to.columnIndex),
      to.columnIndex,
    ];

    return rowCoord.map((row, ind) => ({
      rowIndex: row,
      columnIndex: colCoord[ind],
    }));
  }

  // with needRemove flag equal to `false` chessmen won't be removed (just planning)
  private removeEnemies(
    from: Position,
    to: Position,
    needRemove = true
  ): number {
    const positions = this.getDiagonalPositions(from, to);
    let counter = 0;

    positions.forEach((position) => {
      const positionChessman =
        this.boardState[position.rowIndex][position.columnIndex];

      if (positionChessman && positionChessman.color !== this.turnOrder) {
        counter++;

        if (needRemove) {
          this.boardState[position.rowIndex][position.columnIndex] = null;
        }
      }
    });

    return counter;
  }

  private checkCanMakeFatalMove(fromPosition?: Position): boolean {
    const { rowIndex: curRowInd, columnIndex: curColumnInd } =
      fromPosition ?? this.targetedChessman!;
    const currentPosition = fromPosition ?? this.targetedChessman!;
    const curKingState = this.boardState[curRowInd][curColumnInd]?.isKing!;
    let positionsToCheck: Position[] = [];

    if (!curKingState) {
      positionsToCheck.push({
        rowIndex: curRowInd - 2,
        columnIndex: curColumnInd - 2,
      });
      positionsToCheck.push({
        rowIndex: curRowInd - 2,
        columnIndex: curColumnInd + 2,
      });
      positionsToCheck.push({
        rowIndex: curRowInd + 2,
        columnIndex: curColumnInd - 2,
      });
      positionsToCheck.push({
        rowIndex: curRowInd + 2,
        columnIndex: curColumnInd + 2,
      });
    } else {
      positionsToCheck.push(
        ...this.getDiagonalPositions(currentPosition, {
          rowIndex: curRowInd - this.boardSize,
          columnIndex: curColumnInd - this.boardSize,
        })
      );
      positionsToCheck.push(
        ...this.getDiagonalPositions(currentPosition, {
          rowIndex: curRowInd + this.boardSize,
          columnIndex: curColumnInd - this.boardSize,
        })
      );
      positionsToCheck.push(
        ...this.getDiagonalPositions(currentPosition, {
          rowIndex: curRowInd - this.boardSize,
          columnIndex: curColumnInd + this.boardSize,
        })
      );
      positionsToCheck.push(
        ...this.getDiagonalPositions(currentPosition, {
          rowIndex: curRowInd + this.boardSize,
          columnIndex: curColumnInd + this.boardSize,
        })
      );
    }

    return positionsToCheck.some(
      (position) =>
        this.canMakeMove(position, currentPosition) &&
        this.removeEnemies(
          {
            rowIndex: curRowInd,
            columnIndex: curColumnInd,
          },
          position,
          false
        )
    );
  }

  private swapTurn(): void {
    this.targetedChessman = null;
    this.turnOrder = this.turnOrder === 'white' ? 'black' : 'white';
  }

  private targetChessman(chessman: Chessman, position: Position): void {
    if (this.turnOrder !== chessman.color) {
      return;
    }

    // If we target not the same cell
    if (
      !this.targetedChessman ||
      this.targetedChessman.rowIndex !== position.rowIndex ||
      this.targetedChessman.columnIndex !== position.columnIndex
    ) {
      this.targetedChessman = position;

      return;
    }

    // Otherwise uncheck it
    this.targetedChessman = null;
  }

  private initBoard(): void {
    this.boardState = [];

    for (let row = 0; row < this.boardSize; row++) {
      this.boardState.push([]);

      for (let column = 0; column < this.boardSize; column++) {
        if ((row + column) % 2) {
          if (row <= 2) {
            this.boardState[row].push({ color: 'black', isKing: false });
          } else if (row >= this.boardSize - 3) {
            this.boardState[row].push({ color: 'white', isKing: false });
          } else {
            this.boardState[row].push(null);
          }
        } else {
          this.boardState[row].push(null);
        }
      }
    }
  }
}
