import { Component, Input } from "@angular/core";
import { Chessman } from "./interfaces/chessman.interface";

@Component({
  selector: "app-chessman",
  templateUrl: "./chessman.component.html",
  styleUrls: ["./chessman.component.less"]
})
export class ChessmanComponent {
  @Input()
  chessman: Chessman | null = null;
}
