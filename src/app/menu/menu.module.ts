import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { MenuListComponent } from "./menu-list/menu-list.component";

@NgModule({
  declarations: [
    MenuListComponent
  ],
  imports: [
    BrowserModule,
    CommonModule
  ],
  exports: [MenuListComponent]
})
export class MenuModule {
}
