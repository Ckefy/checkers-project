import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-menu-list",
  templateUrl: "./menu-list.component.html",
  styleUrls: ["./menu-list.component.less"]
})
export class MenuListComponent {
  constructor(public router: Router) {
  }

  createNewGame(): void {
    this.router.navigate(["/game"]);
  }
}
