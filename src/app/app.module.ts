import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { CommonModule } from "@angular/common";
import { AppRoutingModule } from "./app-routing.module";
import { GameModule } from "./game/game.module";
import { HeaderModule } from "./header/header.module";
import { MenuModule } from "./menu/menu.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from "@angular/material/snack-bar";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    GameModule,
    HeaderModule,
    MenuModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
