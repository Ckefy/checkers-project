import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-header-bar",
  templateUrl: "./header-bar.component.html",
  styleUrls: ["./header-bar.component.less"]
})
export class HeaderBarComponent {
  constructor(public router: Router) {
  }

  goHome(): void {
    this.router.navigate(["/menu"]);
  }
}
