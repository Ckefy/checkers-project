import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { HeaderBarComponent } from "./header-bar/header-bar.component";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";

@NgModule({
  declarations: [
    HeaderBarComponent
  ],
  imports: [
    MatIconModule,
    MatButtonModule,
    BrowserModule,
    CommonModule
  ],
  exports: [HeaderBarComponent]
})
export class HeaderModule {
}
