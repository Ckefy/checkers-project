import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ChessBoardComponent } from "./game/chess-board/chess-board.component";
import { MenuListComponent } from "./menu/menu-list/menu-list.component";

const routes: Routes = [
  { path: "game", component: ChessBoardComponent, pathMatch: "full" },
  { path: "menu", component: MenuListComponent },
  { path: "**", redirectTo: "/menu" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
